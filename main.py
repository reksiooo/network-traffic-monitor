"""Application that shows internet speed"""
from abc import ABC, abstractmethod
import time
import psutil


class ValueError(Exception):
    """Exception for traffic value error"""


class NetworkTraffic(ABC):
    """Interface for traffic calculators"""
    @abstractmethod
    def __init__(self) -> None:
        """force to implement init method"""

    @abstractmethod
    def calculate_traffic(self):
        """force to implement calculate_traffic method"""


class DownloadTraffic(NetworkTraffic):
    """Class for download speed calculation"""
    def __init__(self, value='MB') -> None:
        self.value = value


    def calculate_traffic(self):
        """calculate download speed

        Args:
            value (str, optional): Speed value you want to display. Defaults to 'MB'.

        Returns:
            float: calculated speed value
        """

        last_received = psutil.net_io_counters().bytes_recv
        time.sleep(1)
        bytes_received = psutil.net_io_counters().bytes_recv

        if self.value == 'MB':
            new_received = ((bytes_received - last_received) / 1024 / 1024)
            return new_received
        raise ValueError('Bad value for download traffic!')


class UploadTraffic(NetworkTraffic):
    """Class for upload speed calculation"""
    def __init__(self, value='MB') -> None:
        self.value = value


    def calculate_traffic(self):
        """calculate network traffic speed

        Args:
            value (str, optional): Speed value you want to display. Defaults to 'MB'.

        Returns:
            float: calculated speed value
        """

        last_sent = psutil.net_io_counters().bytes_sent
        time.sleep(1)
        bytes_sent = psutil.net_io_counters().bytes_sent

        if self.value == 'MB':
            new_sent = ((bytes_sent - last_sent) / 1024 / 1024)
            return new_sent
        return None


class TotalTraffic(NetworkTraffic):
    """Class for total speed calculation"""
    def __init__(self, value='MB') -> None:
        self.value = value


    def calculate_traffic(self):
        """calculate network traffic speed

        Args:
            value (str, optional): Speed value you want to display. Defaults to 'MB'.

        Returns:
            float: calculated speed value
        """

        last_sent = psutil.net_io_counters().bytes_sent
        last_received = psutil.net_io_counters().bytes_recv
        time.sleep(1)
        bytes_sent = psutil.net_io_counters().bytes_sent
        bytes_received = psutil.net_io_counters().bytes_recv

        if self.value == 'MB':
            new_sent = ((bytes_sent - last_sent) / 1024 / 1024)
            new_received = ((bytes_received - last_received) / 1024 / 1024)
            new_total = new_sent + new_received
            return new_total
        return None


download = DownloadTraffic()
upload = UploadTraffic()
total = TotalTraffic()


# while True:
#     print(f'Down: {download.calculate_traffic():.2f}, Up: {upload.calculate_traffic():.2f}, '
#           f'Total: {total.calculate_traffic():.2f}')
