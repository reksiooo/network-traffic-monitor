from main import DownloadTraffic, ValueError
import pytest


def test_download_traffic_positive():
    #given
    download = DownloadTraffic()

    #when
    result = download.calculate_traffic()

    #then
    assert isinstance(result, float)


def test_download_taffic_negative():
    #given
    download = DownloadTraffic("BjooB")

    #when
    with pytest.raises(ValueError) as error:
        download.calculate_traffic()

    #then
    assert "Bad value for download traffic!" in str(error.value)